# Math-SIBs extension for DIME

This example illustrates how to create a SIB that references a `Math` model, which is a new type of graph model that is not yet available in DIME.

At generation time, from such a Math-SIB executable Java code is generated to be executed from within the code that is generated for the containing Process model.

## Setup the plugin project

Clone this repository to a folder that will be referred to as 'repo location' in the following.

The extension project is supposed to be imported into DIME.
Install DIME from an installer and run it with an empty workspace.

From the context menu of the Project Explorer select 'Import > Existing projects into workspace'.
In the opening dialog, enter your repo location as root directory.
Mark the checkbox of the `info.scce.cinco.product.mathgraph` project and hit 'Finish'.

The project should now have been imported into your workspace.

Notice the registered extension for the extension point `info.scce.dime.modeltrafo` in the plugin.xml file.

```
<extension point="info.scce.dime.modeltrafo">
	<modeltrafo modeltrafosupporter="info.scce.cinco.product.mathgraph.extensionProviders.MathgraphSupporter">
	</modeltrafo>
</extension>
```

In the Project Explorer, navigate to the `model` folder of the `info.scce.cinco.product.mathgraph` project and select 'Generate Cinco Product' from the context menu of the `MathGrahTool.cpd` file.

Wait for the generation process to finish with a 'successful' message.

## Run the product

From the context menu of any of the projects in the Project Explorer select 'Run As > Eclipse Application' to start the product instance.
It can be seen as a new instance of DIME that in addition to the standard bundles contains the `mathgraph` bundles from your workspace, which add the Math-SIBs functionality.

## Setup the runtime project

Head over to the [generic-math-sibs-app](https://gitlab.com/scce/dime-example-apps/generic-math-sibs-app) project and follow the instructions there to setup the demo runtime project and tryout the Math-SIBs.