package info.scce.cinco.product.mathgraph.generator

import info.scce.cinco.product.mathgraph.mathgraph.Addition
import info.scce.cinco.product.mathgraph.mathgraph.Division
import info.scce.cinco.product.mathgraph.mathgraph.MathGraph
import info.scce.cinco.product.mathgraph.mathgraph.Multiplication
import info.scce.cinco.product.mathgraph.mathgraph.Start
import info.scce.cinco.product.mathgraph.mathgraph.Subtraction
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.util.StringInputStream

import static extension info.scce.dime.generator.util.EclipseUtils.*
import static extension info.scce.dime.generator.util.JavaIdentifierUtils.*
import java.io.InputStream
import info.scce.cinco.product.mathgraph.mathgraph.End
import info.scce.cinco.product.mathgraph.mathgraph.Operation
import info.scce.cinco.product.mathgraph.mathgraph.Variable

class MathGraphGenerator {

	static def String getMethodSignature(MathGraph model) {
		return '''«getPackage».«model.modelName.toFirstUpper».calculate'''
	}

	static def void generate(MathGraph model) {
		var IFile file = getFile(model.eResource)
		var IContainer parent = file.parent
		while (!(parent instanceof IProject)) {
			parent = parent.parent
		}
		var IPath outlet = parent.fullPath
		outlet = outlet.removeLastSegments(1).append("target").append("dywa-app").append("app-business").append(
			"target").append("generated-sources").append("info").append("scce").append("spr").append("poc").append(
			"process").append("dime__HYPHEN_MINUS__models").append("math")
		val IFolder outputFolder = parent.getFolder(outlet)
		val outputFile = {
			// TODO create this filename at some central place
			val fileName = '''«model.modelName.toFirstUpper».java'''
			outputFolder.getFile(fileName)
		}
		val CharSequence content = model.content

		outputFolder.mkdirs
		if(!outputFile.exists) outputFile.create
		outputFile.writeContent(content)
	}

	static protected def CharSequence getContent(MathGraph model) {
		val Start start = model.starts.get(0)
		var int i = 0
		return '''
			package «getPackage»;
			
			public class «model.modelName.toFirstUpper» {
				
				public static «model.internalResultTypeName» calculate(«FOR variable : start.variables SEPARATOR ', '»double «variable.name»«ENDFOR») {
					«IF !start.outgoingControlEdges.nullOrEmpty»
						double result«i=i+1» = «(start.outgoingControlEdges.get(0).targetElement as Operation).generateOperation»;
					«ENDIF»
					
					«model.ends.get(0).storedTypeName» result = new «model.ends.get(0).storedTypeName»(result«i»);
					return new «model.internalResultTypeName»(result);
				}
			
				public static class «model.internalResultTypeName» implements info.scce.dime.process.ProcessResult2<Void> {
					private String branchName;
					private String branchId;
					«FOR it : model.ends»
						private «typeName» «branchName»;
						
						public «model.internalResultTypeName»(«typeName» «branchName») {
							this.branchName = "«branchName»";
							this.branchId = "«id.escapeJava»";
							this.«branchName» = «branchName»;
						}
					«ENDFOR»
					
					public String getBranchName() {
						return branchName;
					}
					
					public String getBranchId() {
						return branchId;
					}
					
					«FOR it : model.ends»
						public «typeName» get«typeName»() {
							return «branchName»;
						}
					«ENDFOR»
				}
			
				// model branches.
				«FOR it : model.ends»
					/**
					 * Interface definition for return type of branch <code>«branchName»</code>.
					 */
					public interface «typeName» {
						public double getResult();
					}
					
					/**
					 * Return type of branch <code>«branchName»</code> storing the 
					 * values locally.
					 */
					public static class «storedTypeName» implements «typeName» {
						private double result;
						
						public «storedTypeName»(double result) {
							this.result = result;
						}
						
						public double getResult() {
							return result;
						}
					}
				«ENDFOR»
			}
		'''
	}

	static def String getResultTypeName(MathGraph model) {
		return '''«getPackage».«model.modelName.toFirstUpper».«model.getInternalResultTypeName»'''
	}

	static protected def String getInternalResultTypeName(MathGraph model) {
		return '''«model.modelName.toFirstUpper»Result'''
	}

	static protected def String getTypeName(End end) {
		return '''SuccessReturn'''
	}

	static protected def String getBranchName() {
		return "success"
	}

	static protected def String getStoredTypeName(End end) {
		return '''SuccessReturnStored'''
	}

	static def dispatch String generateOperation(Addition operation) {
		return 
		'''«FOR variable : operation.incomingDataEdges.map[edge | edge.sourceElement] SEPARATOR ' + '»«variable.name»«ENDFOR»'''
	}

	static def dispatch String generateOperation(Subtraction operation) {
		return 
		'''«FOR variable : operation.incomingDataEdges.map[edge | edge.sourceElement] SEPARATOR ' - '»«variable.name»«ENDFOR»'''
	}

	static def dispatch String generateOperation(Multiplication operation) {
		return 
		'''«FOR variable : operation.incomingDataEdges.map[edge | edge.sourceElement] SEPARATOR ' * '»«variable.name»«ENDFOR»'''
	}

	static def dispatch String generateOperation(Division operation) {
		return 
		'''«FOR variable : operation.incomingDataEdges.map[edge | edge.sourceElement] SEPARATOR ' / '»«variable.name»«ENDFOR»'''
	}

	static def String getPackage() {
		return '''info.scce.spr.poc.process.dime__HYPHEN_MINUS__models.interaction.math'''
	}
	
	def static dispatch String getName(Variable variable) {
		return variable.getName
	}
	
	def static dispatch String getName(Operation op) {
		return "result"
	}
	
	static protected def IFile getFile(Resource resource) {
		if (resource !== null) {
			var URI uri = resource.getURI();
			uri = resource.getResourceSet().getURIConverter().normalize(uri);
			var String scheme = uri.scheme();
			if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0))) {
				var StringBuffer platformResourcePath = new StringBuffer();
				for (var int j = 1, var size = uri.segmentCount(); j < size; j++) {
					platformResourcePath.append('/');
					platformResourcePath.append(uri.segment(j));
				}
				return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
			}
		}
		return null;
	}

	static private def InputStream toInputStream(String str) {
		new StringInputStream(str)
	}

	static private def void create(IFile file) {
		file.create("".toInputStream, true, null)
	}

	static private def void writeContent(IFile file, CharSequence content) {
		file.setContents(content.toString.toInputStream, true, true, null)
	}
}
