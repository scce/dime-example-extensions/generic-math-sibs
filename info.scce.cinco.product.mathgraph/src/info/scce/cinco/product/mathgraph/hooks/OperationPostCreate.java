package info.scce.cinco.product.mathgraph.hooks;

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook;
import info.scce.cinco.product.mathgraph.mathgraph.Operation;
import info.scce.cinco.product.mathgraph.mathgraph.ResultVariable;

public class OperationPostCreate extends CincoPostCreateHook<Operation> {

	@Override
	public void postCreate(Operation operationNode) {
		ResultVariable resultVariable = operationNode.newResultVariable(0, 0);
		resultVariable.setName("result");
	}

}
