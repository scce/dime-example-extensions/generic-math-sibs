package info.scce.cinco.product.mathgraph.modellingProviders;

import java.util.ArrayList;
import java.util.List;

import info.scce.cinco.product.mathgraph.mathgraph.MathGraph;
import info.scce.cinco.product.mathgraph.mathgraph.Start;
import info.scce.cinco.product.mathgraph.mathgraph.Variable;
import info.scce.dime.data.data.PrimitiveType;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericOutputBranch;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.GenericPrimitivePort;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;

public class MathSIBModellingProvider implements IGenericSIBBuilder<MathGraph> {

	@Override
	public String getName(MathGraph model) {
		return model.getModelName();
	}

	@Override
	public String getLabel(MathGraph model) {
		return model.getModelName();
	}

	@Override
	public List<GenericPort> getInputPorts(MathGraph model) {
		List<GenericPort> result = new ArrayList<GenericPort>();
		Start start = model.getStarts().get(0);
		for (Variable var : start.getVariables()) {
			result.add(new GenericPrimitivePort(var.getName(), PrimitiveType.REAL));
		}
		return result;
	}

	@Override
	public List<GenericOutputBranch> getOutputBranches(MathGraph model) {
		List<GenericOutputBranch> outputBranches = new ArrayList<GenericOutputBranch>();
		List<GenericPort> ports = new ArrayList<GenericPort>();
		ports.add(new GenericPrimitivePort("result", PrimitiveType.REAL));
		outputBranches.add(new GenericOutputBranch("success", ports));
		return outputBranches;
	}

}
