package info.scce.cinco.product.mathgraph.extensionProviders;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EClass;
import org.osgi.framework.Bundle;
import info.scce.cinco.product.mathgraph.generationProviders.MathgraphGenerationProvider;
import info.scce.cinco.product.mathgraph.mathgraph.MathGraph;
import info.scce.cinco.product.mathgraph.mathgraph.MathgraphPackage;
import info.scce.cinco.product.mathgraph.modellingProviders.MathSIBModellingProvider;
import info.scce.dime.modeltrafo.extensionpoint.buildmodel.IGenericSIBBuilder;
import info.scce.dime.modeltrafo.extensionpoint.trafosupport.IModeltrafoSupporter;
import info.scce.dime.modeltrafo.extensionpoint.transformation.ISIBGenerationProvider;

public class MathgraphSupporter implements IModeltrafoSupporter<MathGraph> {

	private static MathSIBModellingProvider modellingProvider = new MathSIBModellingProvider();
	private static MathgraphGenerationProvider generationProvider = new MathgraphGenerationProvider();
	
	@Override
	public String getModelExtension() {
		return "math";
	}

	@Override
	public String getSIBName() {
		return "MathSIB";
	}

	@Override
	public String getIconPath() {
		Bundle bundle = Platform.getBundle("info.scce.cinco.product.mathgraph");
		URL url = FileLocator.find(bundle, new Path("icons/mathGraph.png"), null);
		try {
			URL fileURL = FileLocator.toFileURL(url);
			return fileURL.getFile();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public IGenericSIBBuilder<MathGraph> getModellingProvider() {
		return modellingProvider;
	}

	@Override
	public EClass getModelType() {
		return MathgraphPackage.eINSTANCE.getMathGraph();
	}

	@Override
	public ISIBGenerationProvider getGenerationProvider() {
		return generationProvider;
	}

}
