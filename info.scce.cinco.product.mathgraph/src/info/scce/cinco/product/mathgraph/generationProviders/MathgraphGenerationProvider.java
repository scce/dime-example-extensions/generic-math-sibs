package info.scce.cinco.product.mathgraph.generationProviders;

import info.scce.cinco.product.mathgraph.generator.MathGraphGenerator;
import info.scce.cinco.product.mathgraph.mathgraph.MathGraph;
import info.scce.dime.modeltrafo.extensionpoint.transformation.IProcessSIBGenerator;

public class MathgraphGenerationProvider implements IProcessSIBGenerator<MathGraph> {

	@Override
	public void generateContent(MathGraph referencedSIBModel) {
		MathGraphGenerator.generate(referencedSIBModel);
	}

	@Override
	public String getMethodCallSignature(MathGraph referencedSIBModel) {
		return MathGraphGenerator.getMethodSignature(referencedSIBModel);
	}

	@Override
	public String getResultTypeName(MathGraph referencedSIBModel) {
		return MathGraphGenerator.getResultTypeName(referencedSIBModel);
	}

}
