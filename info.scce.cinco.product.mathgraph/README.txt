= README =

This is dynamically generated documentation for the Cinco FlowGraph example project with features
selected during project setup.


== Getting Started ==

Generate your Cinco Product: right-click on /info.scce.cinco.product.mathgraph/model/FlowGraphTool.cpd and
select 'Generate Cinco Product'

Start your generated Cinco Product: right-click on /info.scce.cinco.product.mathgraph and
select 'Run as > Eclipse Application'.

Before you can start modeling, you need to create a project: right-click in the Project Explorer and
select 'New > New FlowGraphTool Project', give the project a name and click 'Finish'.

Now start a first FlowGraph model: right-click on your created project and select 'New > New FlowGraph'.

See below for details on the available modeling elements and effects of additional features selected
during project initialization.


== General Features ==

Basic FlowGraph models consist of three types of nodes and two types of edges:

* 'Start' nodes are shown as a green circle and can may have exactly one outgoing 'Transition'

* 'Activity' nodes have attributes 'name' and 'description' and are shown as a blue rectangle
  showing the name.  They can have multiple outgoing 'LabeledTransition' edges, and multiple incoming
  edges of arbitrary type.

* 'End' nodes are shown as a red double circle and can have multiple incoming edges of arbitrary type.


== Additional Features ==	
You have not selected any additional features during project initialization.


